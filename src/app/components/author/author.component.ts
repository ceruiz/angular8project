import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Author } from '../../Models/AuthorModel';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { AuthorService } from '../../services/author.service';

@Component({
  selector: 'app-author',
  templateUrl: './author.component.html',
  styleUrls: ['./author.component.css']
})
export class AuthorComponent implements OnInit {
  authorForm: FormGroup;
  flag = false;
  authors: any[] = [];
  constructor(private AuthorService: AuthorService, private fb: FormBuilder) {
    this.showAuthors();
  }

  ngOnInit() {
    this.authorForm = this.fb.group({
      autId: 0,
      autName: [''],
      autLastName: [''],
      autCountry: [''],
    });
  }
  showAuthors() {
    this.AuthorService.getAuthors()
    .subscribe((resp: any[]) => {
      this.authors = resp;
      this.flag = true;
    });
  }
  onSubmit(formValue: any) {
    const author = new Author();
    author.Id = 0;
    author.autName = formValue.autName;
    author.autLastName = formValue.autLastName;
    author.autCountry = formValue.autCountry;
    this.AuthorService.addAuthor(author)
    .subscribe(resp => {
      this.showAuthors();
    });
  }
  onDelete(id) {
    this.AuthorService.deleteAuthor(id)
    .subscribe( resp => {
      this.showAuthors();
    });
  }
  openModal(id, name, LastName, Country) {
    let modal = document.getElementById('editModal') as HTMLElement;
    modal.style.display = 'block';
    this.authorForm = this.fb.group({
      autId: id,
      autName: name,
      autLastName: LastName,
      autCountry: Country
    });
  }
  onEdit(formValue: any) {
    const author = new Author();
    author.Id = formValue.autId;
    author.autName = formValue.autName;
    author.autLastName = formValue.autLastName;
    author.autCountry = formValue.autCountry;
    this.AuthorService.editAuthor(author)
    .subscribe(resp => {
      this.showAuthors();
    });
    document.getElementById('closeModal').click();
  }
}
