import { Component, OnInit, ViewChild } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from '../../Models/BookModel';
import { Author } from '../../Models/AuthorModel';
import { Category } from '../../Models/CategoryModel';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';
import { AuthorService } from '../../services/author.service';
import { CategoryService } from '../../services/category.service';


@Component({
  selector: 'app-book',
  templateUrl: './book.component.html',
  styleUrls: ['./book.component.css']
})
export class BookComponent implements OnInit {

  bookForm: FormGroup;
  flag = false;
  books: any[] = [];
  author: any;
  category: any;
  constructor(private bookService: BookService, private fb: FormBuilder, private autorS: AuthorService, private cateSer: CategoryService) {
    this.showBooks();
  }

  ngOnInit() {
    this.bookForm = this.fb.group({
      bookId: 0,
      bookName: [''],
      bookCode: [''],
      bookAuthor: [''],
      bookCategory: [''],
      bookPublicationDate: [''],
    });
  }
  showBooks() {
    this.bookService.getBooks()
    .subscribe((resp: any[]) => {
      this.books = resp;
      this.flag = true;
    });
  }
  onSubmit(formValue: any) {
    this.autorS.getAuthor(formValue.bookAuthor)
    .subscribe(resp => {
      this.author = resp;
    });
    this.cateSer.getCategory(formValue.bookAuthor)
    .subscribe(resp => {
      this.category = resp;
    });
    const book = new Book();
    book.Id = 0;
    book.bookName = formValue.bookName;
    book.bookCode = formValue.bookCode;
    book.bookAuthor = this.author;
    book.bookCategory = this.category;
    book.bookPublicationDate = formValue.bookPublicationDate;
    console.log(book);
    this.bookService.addBook(book)
    .subscribe(resp => {
      this.showBooks();
    });
  }
  onDelete(id) {
    this.bookService.deleteBook(id)
    .subscribe( resp => {
      this.showBooks();
    });
  }
  openModal(id, bName, bCode, bAuthor,bCategory, bDate) {
    let modal = document.getElementById('editModal') as HTMLElement;
    modal.style.display = 'block';
    this.bookForm = this.fb.group({
      bookId: id,
      bookName: bName,
      bookCode: bCode,
      bookAuthor: bAuthor,
      bookCategory: bCategory,
      bookPublicationDate: bDate
    });
  }
  onEdit(formValue: any) {
    const book = new Book();
    book.Id = formValue.bookId;
    book.bookName = formValue.bookName;
    book.bookCode = formValue.bookCode;
    book.bookAuthor = formValue.bookAuthor;
    book.bookCategory = formValue.bookCategory;
    book.bookPublicationDate = formValue.bookPublicationDate;
    this.bookService.editBook(book)
    .subscribe(resp => {
      this.showBooks();
    });
    document.getElementById('closeModal').click();
  }

}
