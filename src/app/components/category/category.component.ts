import { Component, OnInit, ViewChild } from '@angular/core';
import { CategoryService } from '../../services/category.service';
import { Category } from '../../Models/CategoryModel';
import { FormControl, FormGroup, FormBuilder, FormArray } from '@angular/forms';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css']
})
export class CategoryComponent implements OnInit {
  categoryForm: FormGroup;
  categoryFormEdit: FormGroup;
  flag = false;
  categories: any[] = [];
  constructor(private CategoryService: CategoryService, private fb: FormBuilder) {
    this.showCategories();
  }

  ngOnInit() {
    this.categoryForm = this.fb.group({
      catId: 0,
      catName: [''],
    });
  }
  showCategories() {
    this.CategoryService.getCategories()
    .subscribe((resp: any[]) => {
      this.categories = resp;
      this.flag = true;
    });
  }
  onSubmit(formValue: any) {
    const category = new Category();
    category.Id = 0;
    category.catName = formValue.catName;
    this.CategoryService.addCategory(category)
    .subscribe(resp => {
      this.showCategories();
    });
  }
  onDelete(id) {
    this.CategoryService.deleteCategory(id)
    .subscribe( resp => {
      this.showCategories();
    });
  }
  openModal(id, name) {
    let modal = document.getElementById('editModal') as HTMLElement;
    modal.style.display = 'block';
    this.categoryForm = this.fb.group({
      catId: id,
      catName: name,
    });
  }
  onEdit(formValue: any) {
    const category = new Category();
    category.Id = formValue.catId;
    category.catName = formValue.catName;
    this.CategoryService.editCategory(category)
    .subscribe(resp => {
      this.showCategories();
    });
    document.getElementById('closeModal').click();
  }
}
