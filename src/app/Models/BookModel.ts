import { Author } from './AuthorModel';
import { Category } from './CategoryModel';
export class Book {
    Id: number;
    bookName: string;
    bookCode: string;
    bookAuthor: Author;
    bookCategory: Category;
    bookPublicationDate: string;
}