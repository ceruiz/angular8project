import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Book } from '../Models/BookModel';

@Injectable({
  providedIn: 'root'
})
export class BookService {
  constructor(private http: HttpClient) { }

  getBook(id) {
    this.http.get('https://localhost:44335/api/Books/' + id)
    .subscribe(resp => {
      console.log(resp);
    });
  }
  addBook(book: Book) {
    return this.http.post('https://localhost:44335/api/Books', book);
  }
  editBook(book: Book) {
    return this.http.put('https://localhost:44335/api/Books/' + book.Id, book);
  }
  getBooks() {
    return this.http.get('https://localhost:44335/api/Books');
  }
  deleteBook(id) {
    return this.http.delete('https://localhost:44335/api/Books/' + id);
  }
}
