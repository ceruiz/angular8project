import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Category } from '../Models/CategoryModel';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {
  constructor(private http: HttpClient) { }

  getCategory(id) {
    return this.http.get('https://localhost:44335/api/Categories/' + id)
  }

  addCategory(category: Category) {
    return this.http.post('https://localhost:44335/api/Categories', category);
  }
  editCategory(category: Category) {
    return this.http.put('https://localhost:44335/api/Categories/' + category.Id, category);
  }
  getCategories() {
    return this.http.get('https://localhost:44335/api/Categories');
  }
  deleteCategory(id) {
    return this.http.delete('https://localhost:44335/api/Categories/' + id);
  }
}
