import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Author } from '../Models/AuthorModel';
@Injectable({
  providedIn: 'root'
})
export class AuthorService {
  constructor(private http: HttpClient) { }

  getAuthor(id) {
    return this.http.get('https://localhost:44335/api/Authors/' + id)
  }

  addAuthor(author: Author) {
    return this.http.post('https://localhost:44335/api/Authors', author);
  }
  editAuthor(author: Author) {
    return this.http.put('https://localhost:44335/api/Authors/' + author.Id, author);
  }
  getAuthors() {
    return this.http.get('https://localhost:44335/api/Authors');
  }
  deleteAuthor(id) {
    return this.http.delete('https://localhost:44335/api/Authors/' + id);
  }
}
